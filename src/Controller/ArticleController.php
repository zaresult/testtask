<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class ArticleController extends AbstractController
{
    /**
     * @Rest\Get("/articles", name="articles")
     *
     * @return JsonResponse
     */
    public function getArticles()
    {
        $articles = $this->getDoctrine()->getRepository(Article::class)->getArticles();

        return $this->json($articles);
    }

    /**
     * @Rest\Get("/article/{id}", name="get_article")
     *
     * @param $id int
     *
     * @return JsonResponse
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getArticle($id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->getArticle($id);
        if (empty($article)) {
            return $this->json('["status":"Not found"]', 404);
        }

        return $this->json($article);
    }

    /**
     * @Rest\Patch("/article/{id}", name="edit_article")
     *
     * @param $id int
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function editArticle($id, Request $request)
    {
        $requestBody = $this->requestBody($request);

        if ($requestBody === null && empty($requestBody)) {
            return $this->json('["status":"Bad request"]', 400);
        }

        $dm = $this->getDoctrine()->getManager();
        $article = $dm->getRepository(Article::class)->getArticle($id);
        $article->setText($requestBody['text']);
        $article->setTitle($requestBody['title']);
        $categories = $dm->getRepository(Category::class)->findByIds($requestBody['categories']);
        foreach ($categories as $category) {
            $article->addCategory($category);
        }
        $article->setUpdatedAt(new DateTime());
        $dm->persist($article);
        $dm->flush();

        return $this->json('["status":"Article edited"]', 200);
    }

    /**
     * @Rest\Delete("/article/{id}", name="delete_article")
     *
     * @param $id int
     *
     * @return JsonResponse
     *
     * @throws /Exception
     */
    public function deleteArticle($id)
    {
        $dm = $this->getDoctrine()->getManager();
        $article = $dm->getRepository(Article::class)->find($id);
        $article->setDeletedAt(new DateTime());
        $dm->persist($article);
        $dm->flush();

        return $this->json('["status":"Article deleted"]', 200);
    }

    /**
     * @Rest\Post("/article", name="add_article")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws /Exception
     */
    public function addArticle(Request $request)
    {
        $requestBody = $this->requestBody($request);

        if ($requestBody === null && empty($requestBody)) {
            return $this->json('["status":"Bad request"]', 400);
        }

        $dm = $this->getDoctrine()->getManager();
        $article = new Article();
        $article->setText($requestBody['text']);
        $article->setTitle($requestBody['title']);
        $dm->persist($article);
        $dm->flush();

        return $this->json('["status":"Article added"]', 200);
    }

    /**
     * @Rest\Get("/search/{searchTerm}", name="search")
     *
     * @param $searchTerm string
     *
     * @return JsonResponse
     */
    public function search($searchTerm)
    {
        $dm = $this->getDoctrine()->getManager();
        $searchResult = $dm->getRepository(Article::class)->search($searchTerm);
        return $this->json($searchResult);
    }

    /**
     * @param $request Request
     *
     * @return mixed
     */
    private function requestBody(Request $request)
    {
        return json_decode($request->getContent(), true);
    }
}
