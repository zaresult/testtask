<?php

namespace App\Controller;

use App\Entity\Category;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractFOSRestController
{
    /**
     * @Rest\Get("/categories", name="categories")
     *
     * @return JsonResponse
     */
    public function getCategories()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->getCategories();

        return $this->json($categories);
    }

    /**
     * @Rest\Get("/category/{id}", name="get_category")
     *
     * @param $id int
     *
     * @return JsonResponse
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getCategory($id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->getCategory($id);
        if (empty($category)) {
            return $this->json('["status":"Not found"]', 404);
        }

        return $this->json($category);
    }

    /**
     * @Rest\Patch("/category/{id}", name="edit_category")
     *
     * @param $id int
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function editCategory($id, Request $request)
    {
        $requestBody = $this->requestBody($request);

        if ($requestBody === null && empty($requestBody)) {
            return $this->json('["status":"Bad request"]', 400);
        }

        $dm = $this->getDoctrine()->getManager();
        $category = $dm->getRepository(Category::class)->getCategory($id);
        $category->setName($requestBody['name']);
        $category->setUpdatedAt(new DateTime());
        $dm->persist($category);
        $dm->flush();

        return $this->json('["status":"Category edited"]', 200);
    }

    /**
     * @Rest\Delete("/category/{id}", name="delete_category")
     *
     * @param $id int
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function deleteCategory($id)
    {
        $dm = $this->getDoctrine()->getManager();
        $category = $dm->getRepository(Category::class)->find($id);
        $category->setDeletedAt(new DateTime());
        $dm->persist($category);
        $dm->flush();

        return $this->json('["status":"Article deleted"]', 200);
    }

    /**
     * @Rest\Post("/category", name="add_category")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     */
    public function addCategory(Request $request)
    {
        $requestBody = $this->requestBody($request);

        if ($requestBody === null && empty($requestBody)) {
            return $this->json('["status":"Bad request"]', 400);
        }

        $dm = $this->getDoctrine()->getManager();
        $category = new Category();
        $category->setName($requestBody['name']);
        $dm->persist($category);
        $dm->flush();

        return $this->json('["status":"Category added"]', 200);
    }

    /**
     * @param $request Request
     *
     * @return mixed
     */
    private function requestBody(Request $request)
    {
        return json_decode($request->getContent(), true);
    }
}
