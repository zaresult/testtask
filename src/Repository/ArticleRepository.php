<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

     /**
      * @return Article[] Returns an array of Article objects
      */
    public function getArticles()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.deletedAt is null')
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $id int
     * @return Article Returns an Article object
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getArticle($id)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.id = :id')
            ->andWhere('a.deletedAt is null')
            ->setParameter('id', $id)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getSingleResult()
        ;
    }

    public function search($searchTerm)
    {
        return $this->createQueryBuilder('a')
            ->where('MATCH_AGAINST(a.title, a.text) AGAINST(:searchTerm boolean)>0')
            ->setParameter('searchTerm', $searchTerm)
            ->getQuery()
            ->getResult()
        ;
    }
}
